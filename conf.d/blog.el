(setq org2blog/wp-blog-alist
      '(("wordpress"
         :url "http://nodinn.me/xmlrpc.php"
         :username "Nodin"
         :default-title "Hello World"
         :default-categories ("org2blog" "emacs")
         :tags-as-categories nil)))
